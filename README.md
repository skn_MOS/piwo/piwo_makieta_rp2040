# Building piwo_makieta

To build piwo_makieta you need to install dependencies such as : </br>
 - https://github.com/raspberrypi/pico-sdk </br>
 - https://gitlab.com/skn_MOS/piwo/libraries/libpiwo </br>

To build makieta you need to type in your console while being in the root folder of piwo_makieta project :</br>
cmake -DPICO_BOARD=pico_w -DPICO_SDK_PATH=<your>/<pico-sdk>/<path> -DPKG_CONFIG_PATH=<your>/<piwo>/<pkgconfig>/<path> -S . -B build  </br>
and then </br>

cd build && make</br>

To flash the pico_w with the software you need to plug in pico_w in boot mode and type in</br>

sudo dd if=./wifi_makieta.uf2 of=/<device path>/<of your pico_w> bs=512 status=progress oflag=sync</br>

While still in the build directory.</br>

Tcp and udp port are the same as in basic config of piwo_player.</br>

To play animations on piwo_makieta you use player as if you were using it on real piwo lightshow.</br>
