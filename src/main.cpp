#include <string.h>
#include <piwo/proto.h>
#include <piwo/protodef.h>

#include "pico/cyw43_arch.h"
#include "pico/stdlib.h"
#include "pico/multicore.h"

#include "hardware/spi.h"
#include "hardware/dma.h"

#include "lwip/pbuf.h"
#include "lwip/tcp.h"
#include "lwip/udp.h"

extern "C"{
    #include "dhcpserver.h"
}

#define PACK_LEN 383
#define UDP_PORT 31337
#define TCP_PORT 1337
#define DEBUG_printf printf

constexpr uint8_t COMMON_PACKET_TYPE_POS          = 0;
constexpr uint8_t FORWARD_W_CID_TYPE              = 0x10;
constexpr uint8_t LIGHTSHOW_TYPE                  = 0x40;
constexpr uint8_t LIGHTSHOW_COLOR_POS             = 4;
constexpr uint16_t COLOR_LEN                      = 120*3;
constexpr uint8_t LIGHTSHOW_FIRST_LA_POS          = 3;
constexpr uint8_t COMMON_TTF_POS                  = 2;
constexpr uint16_t  LED_BUF_SIZE                  = COLOR_LEN * 8;

piwo::net_byte_t buffer[PACK_LEN];
uint8_t color_buff[LED_BUF_SIZE];
uint8_t reset[320];
int count;

typedef struct TCP_SERVER_T_ {
    struct tcp_pcb *server_pcb;
    bool complete;
    ip_addr_t gw;
    async_context_t *context;
} TCP_SERVER_T;

typedef struct TCP_CONNECT_STATE_T_ {
    struct tcp_pcb *pcb;
    int sent_len;
    char headers[128];
    char result[256];
    int header_len;
    int result_len;
    ip_addr_t *gw;
} TCP_CONNECT_STATE_T;

struct udp_pcb *pcb_udp;
struct pbuf *p;
uint dma_color;
uint dma_reset;

void write_led(){
    spi_write_blocking(spi0, reset, 320);
    sleep_us(100);
    spi_write_blocking(spi0, color_buff, LED_BUF_SIZE);
}

void set_color(piwo::net_byte_t buffer[]){
if(buffer[COMMON_PACKET_TYPE_POS] ==  piwo::net_byte_t{ FORWARD_W_CID_TYPE }){
            piwo::raw_packet raw_packet(buffer, PACK_LEN);
            auto forward_w_cid_packet_opt = piwo::forward_w_cid::make_forward_w_cid(raw_packet);
            if(!forward_w_cid_packet_opt.has_value()){
                return;
            }

            int j = 0;
            auto packet = forward_w_cid_packet_opt.value();
            do{
                auto packet_opt = packet.get_next_packet();
                if(packet_opt.has_value()){
                    auto packet = packet_opt.value();
                    if(packet.packet.data()[COMMON_PACKET_TYPE_POS] == piwo::net_byte_t{ LIGHTSHOW_TYPE }){
                        auto lightshow_pack = packet.packet;
                        auto lightshow_opt = piwo::lightshow::make_lightshow(lightshow_pack);
                            if(!lightshow_opt.has_value()){
                                return;
                            }
                        auto lightshow = lightshow_opt.value();
                        uint size = lightshow.get_colors_count();
                        for(uint i=0; i<size; i++){
                            auto color_frame_from_opt = lightshow.get_color(i);
                    	    for (int8_t k = 7; k >= 0; k--) { 
                    	        if ((color_frame_from_opt->get_green() & (1 << k)) == 0) {
                    	        	color_buff[j] = 0b00000111;
                    	        } else {
                    	        	color_buff[j] = 0b00011111;
                    	        }
                    	        j++;
                            }
                    	    for (int8_t k = 7; k >= 0; k--) { 
                    	        if ((color_frame_from_opt->get_red() & (1 << k)) == 0) {
                    	        	color_buff[j] = 0b00000111;
                    	        } else {
                    	        	color_buff[j] = 0b00011111;
                    	        }
                    	        j++; 
                            }
                    	    for (int8_t k = 7; k >= 0; k--) { 
                    	        if ((color_frame_from_opt->get_blue() & (1 << k)) == 0) {
                    	        	color_buff[j] = 0b00000111;
                    	        } else {
                    	        	color_buff[j] = 0b00011111;
                    	        }
                    	        j++; 
                            } 
                        }
                    }else{
                        count--;
                    }   
                }
            } while(forward_w_cid_packet_opt.value().get_next_packet().has_value());
    }
}


static err_t tcp_close_client_connection(TCP_CONNECT_STATE_T *con_state, struct tcp_pcb *client_pcb, err_t close_err) {
    if (client_pcb) {
        assert(con_state && con_state->pcb == client_pcb);
        tcp_arg(client_pcb, NULL);
        tcp_sent(client_pcb, NULL);
        tcp_err(client_pcb, NULL);
        err_t err = tcp_close(client_pcb);
        if (err != ERR_OK) {
            DEBUG_printf("close failed %d, calling abort\n", err);
            tcp_abort(client_pcb);
            close_err = ERR_ABRT;
        }
        if (con_state) {
            free(con_state);
        }
    }
    return close_err;
}

static void tcp_server_close(TCP_SERVER_T *state) {
    if (state->server_pcb) {
        tcp_arg(state->server_pcb, NULL);
        tcp_close(state->server_pcb);
        state->server_pcb = NULL;
    }
}

static err_t tcp_server_sent(void *arg, struct tcp_pcb *pcb, u16_t len) {
    TCP_CONNECT_STATE_T *con_state = (TCP_CONNECT_STATE_T*)arg;
    DEBUG_printf("tcp_server_sent %u\n", len);
    con_state->sent_len += len;
    if (con_state->sent_len >= con_state->header_len + con_state->result_len) {
        DEBUG_printf("all done\n");
        return tcp_close_client_connection(con_state, pcb, ERR_OK);
    }
    return ERR_OK;
}

void udp_server_recv(void *arg, struct udp_pcb *pcb, struct pbuf* p, const ip4_addr *addr, short unsigned int size) {
    if(!p){
        printf("Error receiving packet");
        return ;
    }

    if(count > 0){
        write_led();
        count--;
    }

    if(p->tot_len > 0){
        pbuf_copy_partial(p, buffer, PACK_LEN, 0);
        pbuf_free(p);
        set_color(buffer);
        count++;
    }
} 

static void tcp_server_err(void *arg, err_t err) {
    TCP_CONNECT_STATE_T *con_state = (TCP_CONNECT_STATE_T*)arg;
    if (err != ERR_ABRT) {
        DEBUG_printf("tcp_client_err_fn %d\n", err);
        tcp_close_client_connection(con_state, con_state->pcb, err);
    } }

static err_t tcp_server_accept(void *arg, struct tcp_pcb *client_pcb, err_t err) {
    TCP_SERVER_T *state = (TCP_SERVER_T*)arg;
    if (err != ERR_OK || client_pcb == NULL) {
        DEBUG_printf("failure in accept\n");
        return ERR_VAL;
    }
    DEBUG_printf("client connected\n");

    // Create the state for the connection
    TCP_CONNECT_STATE_T *con_state = (TCP_CONNECT_STATE_T *)calloc(1, sizeof(TCP_CONNECT_STATE_T));
    if (!con_state) {
        DEBUG_printf("failed to allocate connect state\n");
        return ERR_MEM;
    }
    con_state->pcb = client_pcb; // for checking
    con_state->gw = &state->gw;

    // setup connection to client
    tcp_arg(client_pcb, con_state);
    tcp_sent(client_pcb, tcp_server_sent);
    udp_recv(pcb_udp,(udp_recv_fn) udp_server_recv, NULL);
    tcp_err(client_pcb, tcp_server_err);

    return ERR_OK;
}

static bool tcp_server_open(void *arg, const char *ap_name) {
    TCP_SERVER_T *state = (TCP_SERVER_T*)arg;
    DEBUG_printf("starting server on port %d\n", TCP_PORT);

    struct tcp_pcb *pcb = tcp_new_ip_type(IPADDR_TYPE_ANY);
    if (!pcb) {
        DEBUG_printf("failed to create pcb\n");
        return false;
    }

    err_t err = tcp_bind(pcb, IP_ANY_TYPE, TCP_PORT);
    if (err) {
        DEBUG_printf("failed to bind to port %d\n",TCP_PORT);
        return false;
    }

    state->server_pcb = tcp_listen_with_backlog(pcb, 1);
    if (!state->server_pcb) {
        DEBUG_printf("failed to listen\n");
        if (pcb) {
            tcp_close(pcb);
        }
        return false;
    }

    tcp_arg(state->server_pcb, state);
    tcp_accept(state->server_pcb, tcp_server_accept);

    printf("Try connecting to '%s' (press 'd' to disable access point)\n", ap_name);
    return true;
}

int main() {
    stdio_init_all();
    count = 0;
    memset(reset, 0b00000000, 320);

	stdio_init_all();
	spi_init(spi0,6400000);
	spi_set_format(spi0, 8, SPI_CPOL_0, SPI_CPHA_1, SPI_MSB_FIRST);
	gpio_set_function(2, GPIO_FUNC_SPI); //SCK
	gpio_set_function(3, GPIO_FUNC_SPI); // TX

    TCP_SERVER_T *state = (TCP_SERVER_T *)calloc(1, sizeof(TCP_SERVER_T));
    if (!state) {
        DEBUG_printf("failed to allocate state\n");
        return 1;
    }

    if (cyw43_arch_init()) {
        DEBUG_printf("failed to initialise\n");
        return 1;
    }

    const char *ap_name = "makieta_t16";
    const char *password = "password";

    p = pbuf_alloc(PBUF_TRANSPORT, PACK_LEN, PBUF_POOL);

    pcb_udp = udp_new();
    if(pcb_udp == NULL){
        exit(1);
    }

    if(udp_bind(pcb_udp, IP_ADDR_ANY, 31337) != ERR_OK){
        exit(1);
    }

    cyw43_arch_enable_ap_mode(ap_name, password, CYW43_AUTH_WPA2_AES_PSK);

    ip4_addr_t mask;
    IP4_ADDR(ip_2_ip4(&state->gw), 192, 168, 4, 1);
    IP4_ADDR(ip_2_ip4(&mask), 255, 255, 255, 0);

    // Start the dhcp server
    dhcp_server_t dhcp_server;
    dhcp_server_init(&dhcp_server, &state->gw, &mask);


    if (!tcp_server_open(state, ap_name)) {
        DEBUG_printf("failed to open server\n");
        return 1;
    }

    state->complete = false;
    while(!state->complete) {
    }
    tcp_server_close(state);
    dhcp_server_deinit(&dhcp_server);
    cyw43_arch_deinit();
    pbuf_free(p);
    return 0;
}
